const axios = require("axios");
const tmi = require("tmi.js");
var player = "YZ blitzeN"; // Change game name here
var leaderboardRank; // the leaderboard rank
var rankedRating; // the ranked rating
var result;
var url = process.env.API_URL;
const getLB = function(start) {
  var found = false;
  var startIndex = start;
  var leaderboard;
  return new Promise((resolve, reject) => {
    axios
      .get(url + startIndex, {
        headers: {
          "X-Riot-Token": process.env.X_RIOT_TOKEN
        }
      })
      .then(response => {
        resolve({ response });
      });
  });
};

// Define TMI configuration options
const tmiOpts = {
  connection: { reconnect: true },
  identity: {
    username: process.env.USERNAME,
    password: process.env.OAUTH_PASSWORD
  },
  channels: ["blitzennnn"]
};
// Create a TMI client with our options
const client = new tmi.client(tmiOpts);
client.connect();
// TMI event that runs whenever the app connects to Twitch chat
client.on("connected", (addr, port) => {
  console.log("* Connected to Twitch through TMI", addr, port);
});

// TMI event that runs whenever the app receives a message
client.on("message", (channel, user, msg, self) => {
  var found = false;
  var startIndex = 0;
  if (self) {
    return;
  } // Ignore messages from the bot
  if (msg.toLowerCase().startsWith("!rank")) {
    (async () => {
      while (!found) {
        console.log(
          "getrank ",
          await getLB(startIndex).then(msg => {
            if (startIndex >= msg.response.data.totalPlayers) {
              return "pas dans la leaderboard frerot";
            }
            // get player list
            var leaderboard = msg.response.data.players;
            if (leaderboard) {
              // browse list
              for (var i = 0; i < 99; i++) {
                if (leaderboard[i].gameName == player) {
                  found = true;
                  leaderboardRank = leaderboard[i].leaderboardRank;
                  rankedRating = leaderboard[i].rankedRating;
                }
                if (found) {
                  result =
                    "Score 🔥 : " +
                    rankedRating +
                    " / Rang 🏆: " +
                    leaderboardRank;
                  if (leaderboardRank <= 500) {
                    result += " RADIANT MA GUEULE !";
                  } else {
                    result += " IMMORTAL";
                  }
                  client.say(channel, result);
                  return result;
                }
              }
            }
            if (!found) {
              startIndex += 100;
              getLB(startIndex);
            }
            if (found) {
              console.log(result);
            }
          })
        );
      }
    })();
  }
});
